package com.blog.apigateway.configuration;

import com.blog.apigateway.filter.AppFilter;
import com.blog.apigateway.filter.UserFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class ApiGatewayConfiguration {
    private final AppFilter filter;
    private final UserFilter userFilter;

    @Bean
    RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
        return builder.routes()
                .route(r -> r.path("/api/v1/auth/**")
                        .and().method("POST", "PUT", "DELETE", "GET")
                        .filters(f -> f.filter(filter))
                        .uri("http://localhost:8091/"))
                .route(r -> r.path("/api/v1/stories/**")
                        .and().method("POST", "PUT", "DELETE")
                        .filters(f -> f.filter(filter).filter(userFilter))
                        .uri("http://localhost:8092/"))
                .route(r -> r.path("/api/v1/stories/**")
                        .and().method("GET")
                        .uri("http://localhost:8092/"))
                .build();
    }
}