package com.blog.stories.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.time.OffsetDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UpdateStoryResponseDTO implements Serializable {
    private String title;
    private String story;
    @JsonProperty("Last Updated")
    private OffsetDateTime updatedAt;
}
