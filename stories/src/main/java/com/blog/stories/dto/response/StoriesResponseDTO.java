package com.blog.stories.dto.response;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import java.io.Serializable;
import java.time.OffsetDateTime;

@JsonIgnoreProperties(allowGetters = true)
@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Getter
@Setter @ToString
public class StoriesResponseDTO implements Serializable {

    private Long users_id;
    private String title;
    private String story;
    @JsonProperty("Last Updated")
    private OffsetDateTime updatedAt;

}
