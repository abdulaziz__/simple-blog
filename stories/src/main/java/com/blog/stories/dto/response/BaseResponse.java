package com.blog.stories.dto.response;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseResponse<T> implements Serializable {
    private String status;
    private String message;
    private T data;
    private Integer page;
    private Integer size;
    private Integer totalPage;
    private Long totalItem;
}