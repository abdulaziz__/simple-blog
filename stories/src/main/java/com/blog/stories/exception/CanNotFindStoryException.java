package com.blog.stories.exception;

public class CanNotFindStoryException extends RuntimeException{
    public CanNotFindStoryException(String message){
        super(message);
    }
}
