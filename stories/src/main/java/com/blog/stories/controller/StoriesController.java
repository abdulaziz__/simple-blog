package com.blog.stories.controller;

import com.blog.stories.dto.request.StoriesRequestDTO;
import com.blog.stories.dto.request.UpdateStoryRequestDTO;
import com.blog.stories.dto.response.BaseResponse;
import com.blog.stories.dto.response.StoriesResponseDTO;
import com.blog.stories.dto.response.UpdateStoryResponseDTO;
import com.blog.stories.services.IStoriesServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/stories")
@Slf4j
public class StoriesController {

    @Autowired
    IStoriesServices storiesServices;

    private static final String STATUS = "Success";

    @PostMapping()
    public ResponseEntity<BaseResponse<StoriesResponseDTO>> makeStory(@RequestBody StoriesRequestDTO storiesRequestDTO){
        return new ResponseEntity<>(BaseResponse.<StoriesResponseDTO>builder()
                .status(STATUS)
                .message("Successfully make story")
                .data(storiesServices.makeStory(storiesRequestDTO))
                .build(),HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<BaseResponse<List<StoriesResponseDTO>>> seeAllStories(@RequestParam(defaultValue = "") String search, Pageable pageable){
        BaseResponse<List<StoriesResponseDTO>> seeAllStories = storiesServices.seeAllStories(search, pageable);
        return new ResponseEntity<>(seeAllStories, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse<StoriesResponseDTO>> readStory(@PathVariable Long id){
        StoriesResponseDTO readStory = storiesServices.readStory(id);
        return new ResponseEntity<>(BaseResponse.<StoriesResponseDTO>builder()
                .status(STATUS)
                .message("Success to get story")
                .data(readStory).build(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<BaseResponse<UpdateStoryResponseDTO>> updateStory(@PathVariable Long id, @RequestBody UpdateStoryRequestDTO updateStoryRequestDTO){
        UpdateStoryResponseDTO updateStoryResponseDTO = storiesServices.updateStory(id, updateStoryRequestDTO);
        return new ResponseEntity<>(BaseResponse.<UpdateStoryResponseDTO>builder()
                .status(STATUS)
                .message("Story updated")
                .data(updateStoryResponseDTO).build(), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<BaseResponse<Object>> deleteStory(@PathVariable Long id){
        storiesServices.deleteStory(id);
        return new ResponseEntity<>(BaseResponse.builder()
                .status(STATUS)
                .message("Story deleted")
                .build(),HttpStatus.OK);
    }

}