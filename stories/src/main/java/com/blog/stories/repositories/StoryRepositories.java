package com.blog.stories.repositories;

import com.blog.stories.entities.Stories;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@EnableJpaRepositories
@Repository
public interface StoryRepositories extends JpaRepository<Stories, Long> {

    Optional<Stories> findStoriesById(Long id);

    @Query(
            value = "SELECT * FROM stories WHERE title ILIKE CONCAT ('%', :search, '%') AND deleted_at IS NULL ORDER BY updated_at DESC ",
            nativeQuery = true
    )
    Page<Stories> findAllStories(@Param("search") String search,Pageable pageable);
}