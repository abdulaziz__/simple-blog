package com.blog.stories.entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Table(name = "stories")
@Setter
@Getter
public class Stories {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Long users_id;

    @Column(nullable= false)
    private String title;

    @Column(nullable= false)
    private String story;

    @Column(name = "created_at", columnDefinition = "TIMESTAMPTZ NOT NULL")
    @CreationTimestamp
    private OffsetDateTime createdAt = OffsetDateTime.now();

    @Column(name = "updated_at", columnDefinition = "TIMESTAMPTZ NOT NULL")
    @UpdateTimestamp
    private OffsetDateTime updatedAt = OffsetDateTime.now();

    @Column(name = "deleted_at", columnDefinition = "TIMESTAMPTZ")
    private OffsetDateTime deletedAt;

}
