package com.blog.stories.services;

import com.blog.stories.dto.request.StoriesRequestDTO;
import com.blog.stories.dto.request.UpdateStoryRequestDTO;
import com.blog.stories.dto.response.BaseResponse;
import com.blog.stories.dto.response.StoriesResponseDTO;
import com.blog.stories.dto.response.UpdateStoryResponseDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface IStoriesServices {

    StoriesResponseDTO makeStory(StoriesRequestDTO storiesRequestDTO);
    BaseResponse<List<StoriesResponseDTO>> seeAllStories(String query, Pageable pageable);
    UpdateStoryResponseDTO updateStory(Long id, UpdateStoryRequestDTO updateStoryRequestDTO);
    StoriesResponseDTO deleteStory(Long id);
    StoriesResponseDTO readStory (Long id);

}
