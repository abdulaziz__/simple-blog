package com.blog.stories.services.impl;


import com.blog.stories.dto.request.StoriesRequestDTO;
import com.blog.stories.dto.request.UpdateStoryRequestDTO;
import com.blog.stories.dto.response.BaseResponse;
import com.blog.stories.dto.response.StoriesResponseDTO;
import com.blog.stories.dto.response.UpdateStoryResponseDTO;
import com.blog.stories.entities.Stories;
import com.blog.stories.exception.CanNotFindStoryException;
import com.blog.stories.repositories.StoryRepositories;
import com.blog.stories.services.IStoriesServices;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.time.OffsetDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class StoriesServices implements IStoriesServices {

    private final StoryRepositories storyRepositories;
    private final ModelMapper modelMapper;

    private static final String ERROR = "Story Not Found";

    private static final String MESSAGE = "Failed to get story";

    @Override
    @Transactional
    public StoriesResponseDTO makeStory(StoriesRequestDTO storiesRequestDTO) {

        Stories stories = modelMapper.map(storiesRequestDTO, Stories.class);
        Stories stories1 = storyRepositories.save(stories);
        StoriesResponseDTO storiesResponseDTO = modelMapper.map(stories1, StoriesResponseDTO.class);
        return storiesResponseDTO;
    }

    @Override
    public BaseResponse<List<StoriesResponseDTO>> seeAllStories(String search, Pageable pageable) {
        Page<Stories> stories = storyRepositories.findAllStories(search, pageable);
        List<StoriesResponseDTO> storiesResponseDTOList = new ArrayList<>();
        for (Stories s : stories.getContent()) {
            StoriesResponseDTO storiesResponseDTO = modelMapper.map(s, StoriesResponseDTO.class);
            storiesResponseDTOList.add(storiesResponseDTO);
        }
        log.info("Success to get all story with query or pagination");
        return BaseResponse.<List<StoriesResponseDTO>>builder()
                .status("Success")
                .message("Successfully to get all stories")
                .data(storiesResponseDTOList)
                .page(stories.getNumber())
                .size(stories.getSize())
                .totalPage(stories.getTotalPages())
                .totalItem(stories.getTotalElements())
                .build();
    }

    @Override
    public UpdateStoryResponseDTO updateStory(Long id, UpdateStoryRequestDTO updateStoryRequestDTO) {
        Optional<Stories> stories = storyRepositories.findStoriesById(id);
        if (stories.isPresent()){
            log.info("Found Story");
            Stories updateStory = stories.get();
            modelMapper.map(updateStoryRequestDTO, updateStory);

            Stories newStory = storyRepositories.save(updateStory);

            log.info("Success to update story");
            return modelMapper.map(newStory, UpdateStoryResponseDTO.class);
        }else {
            log.error(ERROR);
            throw new CanNotFindStoryException(MESSAGE);
        }
    }

    @Override
    public StoriesResponseDTO deleteStory(Long id) {
        Optional<Stories> stories = storyRepositories.findStoriesById(id);
        if (stories.isEmpty()) {
            log.error(ERROR);
            throw new CanNotFindStoryException(MESSAGE);
        }

        Stories stories1 = stories.get();
        stories1.setDeletedAt(OffsetDateTime.now());
        log.info("Success to delete story");
        storyRepositories.save(stories1);

        return modelMapper.map(stories1, StoriesResponseDTO.class);
    }

    @Override
    public StoriesResponseDTO readStory(Long id) {
        Optional<Stories> stories = storyRepositories.findStoriesById(id);
        if (stories.isEmpty()) {
            log.error(ERROR);
            throw new CanNotFindStoryException(MESSAGE);
        }
        return modelMapper.map(stories, StoriesResponseDTO.class);
    }
}
