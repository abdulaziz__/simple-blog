package com.blog.stories.handler;

import com.blog.stories.dto.response.BaseResponse;
import com.blog.stories.exception.CanNotFindStoryException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class GlobalExceptionHandler {

    String failed = "Failed";

    @ExceptionHandler(value = {CanNotFindStoryException.class})
    public ResponseEntity<Object> handleStoryNotFound(CanNotFindStoryException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

}
