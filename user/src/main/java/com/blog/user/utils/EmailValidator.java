package com.blog.user.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
    private EmailValidator(){
        throw new IllegalStateException("Utility Class");
    }
    private static final String EMAIL_PATTERN =
            "^[a-zA-Z0-9_!#$%&'*.+/=?`{|}~^-]+@"
                    + "[a-zA-Z0-9-]+\\.[a-zA-Z0-9-]+$";

    private static final Pattern pattern = Pattern.compile(EMAIL_PATTERN);

    public static boolean isValid(final String email) {
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
}
