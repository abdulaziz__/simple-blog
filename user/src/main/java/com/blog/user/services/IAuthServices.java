package com.blog.user.services;

import com.blog.user.dto.request.RegisterRequestDTO;
import com.blog.user.dto.request.UserRequestDTO;
import com.blog.user.dto.response.UserResponseDTO;
import org.springframework.security.core.Authentication;

public interface IAuthServices {

    public String generatedToken(Authentication authentication);
    public UserRequestDTO decodeToken(String token);

    UserResponseDTO registerUser(RegisterRequestDTO registerRequestDTO);
}
