package com.blog.user.services.impl;

import com.blog.user.dto.request.RegisterRequestDTO;
import com.blog.user.dto.request.UserRequestDTO;
import com.blog.user.dto.response.UserResponseDTO;
import com.blog.user.entities.Users;
import com.blog.user.exception.*;
import com.blog.user.repositories.UserRepositories;
import com.blog.user.services.IAuthServices;
import com.blog.user.utils.EmailValidator;
import com.blog.user.utils.PasswordValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class AuthServices implements IAuthServices {

    @Autowired
    JwtEncoder jwtEncoder;
    @Autowired
    JwtDecoder jwtDecoder;
    @Autowired
    UserRepositories userRepositories;
    @Autowired
    ModelMapper modelMapper;
    @Autowired
    PasswordEncoder encoder;

    @Override
    public String generatedToken(Authentication authentication) {
        Instant now = Instant.now();
        String scope = authentication
                .getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(""));
        JwtClaimsSet claims = JwtClaimsSet.builder()
                .issuer("self")
                .issuedAt(now)
                .expiresAt(now.plus(30, ChronoUnit.DAYS))
                .subject(authentication.getName())
                .claim("scope", scope)
                .build();
        log.info("Success to generate token");
        return this.jwtEncoder.encode(JwtEncoderParameters.from(claims)).getTokenValue();
    }

    @Override
    public UserRequestDTO decodeToken(String token) {
        String newToken = token.split(" ")[1];

        Jwt jwtToken = jwtDecoder.decode(newToken);

        String data = jwtToken.getSubject();

        Optional<Users> user = userRepositories.findUsersByUsername(data);

        log.info("Check if user is exist");
        if (user.isPresent()) {
            log.info("Found User");
            return modelMapper.map(user.get(), UserRequestDTO.class);
        } else {
            log.info("User not found!");
            throw new CanNotFindUserException("User not found!");
        }
    }

    @Override
    public UserResponseDTO registerUser(RegisterRequestDTO registerRequestDTO) {
        Users users = modelMapper.map(registerRequestDTO, Users.class);
        Optional<Users> emailOptional = userRepositories.findUsersByEmail(users.getEmail());
        Optional<Users> usernameExist = userRepositories.findUsersByUsername(users.getUsername());
        if (!EmailValidator.isValid(users.getEmail())) {
            throw new EmailNotValidException("Email Is Not Valid");
        }
        if (emailOptional.isPresent()) {
            log.info("Email Already Exist!");
            throw new UserEmailNotFoundException("Email Already Exist!");
        }
        if (usernameExist.isPresent()) {
            log.info("Username Already Exist!");
            throw new UsernameAlreadyExistException("Username Already Exist!");
        }
        if (!PasswordValidator.isValid(users.getPassword())) {
            log.info("Your password must be at least 8 characters including a lowercase letter, an uppercase letter, a symbols, and a number");
            throw new PasswordNotValidException("Your password must be at least 8 characters including a lowercase letter, an uppercase letter, a symbols, and a number");
        }

        users.setPassword(encoder.encode(users.getPassword()));
        Users user1 = userRepositories.save(users);
        return modelMapper.map(user1, UserResponseDTO.class);
    }
}
