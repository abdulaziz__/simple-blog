package com.blog.user.services.impl;

import com.blog.user.entities.Users;
import com.blog.user.exception.CanNotFindUserException;
import com.blog.user.repositories.UserRepositories;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@RequiredArgsConstructor
@Service
@Slf4j
public class JpaUserDetailServices implements UserDetailsService{
    private final UserRepositories userRepositories;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Users> users = userRepositories.findUsersByUsername(username);
        if (users.isEmpty()) {
            log.info("User Not Found");
            throw new CanNotFindUserException("User Not Found");
        }
        return users.get();
    }
}
