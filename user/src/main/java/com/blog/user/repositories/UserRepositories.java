package com.blog.user.repositories;

import com.blog.user.entities.Users;
import org.springframework.data.jpa.repository.JpaRepository;


import java.util.Optional;

public interface UserRepositories extends JpaRepository<Users, Long> {

    Optional<Users> findUsersByUsername(String username);
    Optional<Users> findUsersByEmail(String email);

}
