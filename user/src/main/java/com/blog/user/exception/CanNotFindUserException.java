package com.blog.user.exception;

public class CanNotFindUserException extends RuntimeException{
    public CanNotFindUserException(String message){
        super(message);
    }
}
