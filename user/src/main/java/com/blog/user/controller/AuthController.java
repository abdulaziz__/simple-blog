package com.blog.user.controller;

import com.blog.user.dto.request.LoginRequestDTO;
import com.blog.user.dto.request.RegisterRequestDTO;
import com.blog.user.dto.request.UserRequestDTO;
import com.blog.user.dto.response.BaseResponse;
import com.blog.user.dto.response.UserResponseDTO;
import com.blog.user.exception.PasswordNotValidException;
import com.blog.user.services.IAuthServices;
import com.blog.user.services.impl.AuthServices;
import com.blog.user.utils.EmailValidator;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final IAuthServices authServices;
    private final AuthenticationManager authenticationManager;

    public AuthController(AuthServices authServices, AuthenticationManager authenticationManager) {
        this.authServices = authServices;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping("/register")
    public ResponseEntity<BaseResponse> registerUser(@RequestBody RegisterRequestDTO registerRequestDTO){
        return new ResponseEntity<>(BaseResponse.<UserResponseDTO>builder()
                .status("Success")
                .message("User Registered")
                .data(authServices.registerUser(registerRequestDTO))
                .build(), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<BaseResponse<String>> token(@RequestBody LoginRequestDTO loginRequestDTO) {
        String email = loginRequestDTO.getEmail();
        if (!EmailValidator.isValid(loginRequestDTO.getEmail())) {
            throw new PasswordNotValidException("The type must be email");
        }
        String[] username = email.split("@");

        Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username[0], loginRequestDTO.getPassword()));

        String token = authServices.generatedToken(authentication);
        return new ResponseEntity<>(BaseResponse.<String>builder()
                .status("Success")
                .message("User successfully login")
                .data(token)
                .build(), HttpStatus.OK);
    }

    @GetMapping("/info")
    public ResponseEntity<BaseResponse<UserRequestDTO>> getUserInfo(@RequestHeader(name ="Authorization") String bearerToken){
        UserRequestDTO userResponseDTO = authServices.decodeToken(bearerToken);

        return new ResponseEntity<>(BaseResponse.<UserRequestDTO>builder()
                .status("Success")
                .message("User successfully login")
                .data(userResponseDTO)
                .build(), HttpStatus.OK);
//        return new ResponseEntity<>(userResponseDTO, HttpStatus.OK);
    }

}
