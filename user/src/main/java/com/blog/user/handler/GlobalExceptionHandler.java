package com.blog.user.handler;

import com.blog.user.dto.response.BaseResponse;
import com.blog.user.exception.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    String failed = "Failed";

    @ExceptionHandler(value = RuntimeException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<Object> handleServerError(CanNotFindUserException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(value = {CanNotFindUserException.class})
    public ResponseEntity<Object> handleUser(CanNotFindUserException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {UserEmailNotFoundException.class})
    public ResponseEntity<Object> handleEmailNotFound(UserEmailNotFoundException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {PasswordNotValidException.class})
    public ResponseEntity<Object> handlePasswordNotValid(PasswordNotValidException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {UsernameAlreadyExistException.class})
    public ResponseEntity<Object> handleUsernameAlreadyExistException(UsernameAlreadyExistException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {EmailNotValidException.class})
    public ResponseEntity<Object> handleEmailNotValidException(EmailNotValidException e){
        return new ResponseEntity<>(BaseResponse.builder()
                .message(e.getMessage()).status(failed).build(), HttpStatus.BAD_REQUEST);
    }

}
